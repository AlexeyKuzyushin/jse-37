package ru.rencredit.jschool.kuzyushin.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.api.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Exception_Exception;
import ru.rencredit.jschool.kuzyushin.tm.event.ConsoleEvent;
import ru.rencredit.jschool.kuzyushin.tm.listener.AbstractListener;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.ProjectDTO;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.ProjectEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;

import java.util.List;

@Component
public final class ProjectListListener extends AbstractListener {

    @NotNull
    private final ProjectEndpoint projectEndpoint;

    @Autowired
    public ProjectListListener(
            final @NotNull ProjectEndpoint projectEndpoint,
            final @NotNull ISessionService sessionService
    ) {
        super(sessionService);
        this.projectEndpoint = projectEndpoint;
    }

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project list";
    }

    @Override
    @EventListener(condition = "@projectListListener.name() == #event.command")
    public void handler(final ConsoleEvent event) {
        System.out.println("[LIST PROJECTS]");
        @Nullable final SessionDTO sessionDTO = sessionService.getCurrentSession();
        @NotNull final List<ProjectDTO> projectsDTO = projectEndpoint.findAllProjects(sessionDTO);
        int index = 1;
        for (ProjectDTO projectDTO: projectsDTO) {
            System.out.println(index + ". " + projectDTO.getName());
            index++;
        }
        System.out.println("[OK]");
    }
}
