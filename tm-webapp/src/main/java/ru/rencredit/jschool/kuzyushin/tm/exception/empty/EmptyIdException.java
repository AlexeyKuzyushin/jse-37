package ru.rencredit.jschool.kuzyushin.tm.exception.empty;

import ru.rencredit.jschool.kuzyushin.tm.exception.AbstractException;

public class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("Error! ID is empty...");
    }
}
