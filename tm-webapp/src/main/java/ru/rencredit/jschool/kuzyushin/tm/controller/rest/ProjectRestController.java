package ru.rencredit.jschool.kuzyushin.tm.controller.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IProjectService;
import ru.rencredit.jschool.kuzyushin.tm.dto.ProjectDTO;

import java.util.List;

@RestController
@RequestMapping(value="/rest/projects")
public class ProjectRestController {

    @NotNull
    private final IProjectService projectService;

    @Autowired
    public ProjectRestController(final @NotNull IProjectService projectService
    ) {
        this.projectService = projectService;
    }

    @PostMapping(value = "/create")
    public void createProject(@RequestBody final @Nullable ProjectDTO projectDTO) {
        projectService.create(
                projectDTO.getUserId(),
                projectDTO.getName(),
                projectDTO.getDescription());
    }

    @PutMapping(value = "/update")
    public void updateProject(@RequestBody final @Nullable  ProjectDTO projectDTO) {
        projectService.updateById(
                projectDTO.getUserId(),
                projectDTO.getId(),
                projectDTO.getName(),
                projectDTO.getDescription());
    }

    @NotNull
    @GetMapping(value = "/count", produces = MediaType.APPLICATION_JSON_VALUE)
    public Long countOfProjects() {
        return projectService.count();
    }

    @Nullable
    @GetMapping(value = "/find/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ProjectDTO findProject(@PathVariable("id") final @Nullable String id) {
        return ProjectDTO.toDTO(projectService.findProjectById(id));
    }

    @NotNull
    @GetMapping(value = "/findAll", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ProjectDTO> findAllProjects() {
        return projectService.findAll();
    }

    @DeleteMapping(value = "/remove/{id}")
    public void deleteProject(@PathVariable("id") final @Nullable String id) {
        projectService.removeProjectById(id);
    }
}
