package ru.rencredit.jschool.kuzyushin.tm.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.constant.DateConstant;
import ru.rencredit.jschool.kuzyushin.tm.entity.Task;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TaskDTO extends AbstractEntityDTO {

    public static final long serialVersionUID = 1;

    @NotNull
    private String name;

    @Nullable
    private String description;

    @NotNull
    private String userId;

    @Nullable
    private String projectId;

    @Nullable
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern= DateConstant.DATE_FORMAT)
    private Date startDate;

    @Nullable
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern= DateConstant.DATE_FORMAT)
    private Date finishDate;

    @Nullable
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern= DateConstant.DATE_FORMAT)
    private Date creationDate;

    @Override
    public String toString() {
        return getId() + ": " + name;
    }

    @Nullable
    public static TaskDTO toDTO(final @Nullable Task task) {
        if (task == null) return null;
        return new TaskDTO(task);
    }

    @NotNull
    public static List<TaskDTO> toDTO(final @Nullable List<Task> tasks) {
        if (tasks == null || tasks.isEmpty()) return Collections.emptyList();
        @NotNull final List<TaskDTO> result = new ArrayList<>();
        for (@Nullable final Task task: tasks) {
            if (task == null) continue;
            result.add(new TaskDTO(task));
        }
        return result;
    }

    public TaskDTO(final @Nullable Task task) {
        if (task == null) return;
        id = task.getId();
        name = task.getName();
        description = task.getDescription();
        userId = task.getUser().getId();
        if (task.getProject() != null) projectId = task.getProject().getId();
        if (task.getCreationTime() != null) creationDate = task.getCreationTime();
        if (task.getStartDate() != null) startDate = task.getStartDate();
        if (task.getFinishDate() != null) finishDate = task.getFinishDate();
    }
}
