package ru.rencredit.jschool.kuzyushin.tm.controller.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.rencredit.jschool.kuzyushin.tm.api.service.ITaskService;
import ru.rencredit.jschool.kuzyushin.tm.dto.TaskDTO;

import java.util.List;

@RestController
@RequestMapping(value="/rest/tasks")
public class TaskRestController {

    private final ITaskService taskService;

    @Autowired
    public TaskRestController(final @NotNull ITaskService taskService) {
        this.taskService = taskService;
    }

    @PostMapping(value = "/create")
    public void createTask(@RequestBody final @Nullable TaskDTO taskDTO) {
        taskService.create(
                taskDTO.getUserId(),
                taskDTO.getName(),
                taskDTO.getProjectId(),
                taskDTO.getDescription());
    }

    @PutMapping(value = "/update")
    public void updateTask(@RequestBody final @Nullable TaskDTO taskDTO) {
        taskService.updateById(
                taskDTO.getUserId(),
                taskDTO.getId(),
                taskDTO.getName(),
                taskDTO.getDescription());
    }

    @NotNull
    @GetMapping(value = "/count", produces = MediaType.APPLICATION_JSON_VALUE)
    public Long countOfTasks() {
        return taskService.count();
    }

    @Nullable
    @GetMapping(value = "/find/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public TaskDTO findTask(@PathVariable("id") final @Nullable String id) {
        return TaskDTO.toDTO(taskService.findTaskById(id));
    }

    @NotNull
    @GetMapping(value = "/findAll", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TaskDTO> findAllTasks() {
        return taskService.findAll();
    }

    @DeleteMapping(value = "/remove/{id}")
    public void deleteTask(@PathVariable("id") final @Nullable String id) {
        taskService.removeTaskById(id);
    }
}
