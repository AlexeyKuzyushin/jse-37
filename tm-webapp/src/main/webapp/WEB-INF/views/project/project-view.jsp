<jsp:include page="../include/_header.jsp" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<h1>PROJECT INFO</h1>
<table width="100%" border="0" style="border-collapse: collapse">
    <tr>
        <td width="20%">
            <p>
                <div style="margin-bottom: 5px">ID:</div>
                <div><input type="text" readonly="true" name="id" value="${project.id}"></div>
            </p>
            <p>
                <div style="margin-bottom: 5px">NAME:</div>
                <div><input type="text" readonly="true" name="name" value="${project.name}"></div>
            </p>
            <p>
                <div style="margin-bottom: 5px">DESCRIPTION:</div>
                <div><input type="text" readonly="true" name="description" value="${project.description}"></div>
            </p>
            <p>
                <div style="margin-bottom: 5px">CREATION-TIME:</div>
                <div><input type="text" readonly="true" name="creationTime" value="${project.creationTime}"></div>
            </p>
            <p>
                <div style="margin-bottom: 5px">START-DATE:</div>
                <div><input type="text" readonly="true" name="startDate" value="${project.startDate}"></div>
            </p>
            <p>
                <div style="margin-bottom: 5px">FINISH-DATE:</div>
                <div><input type="text" readonly="true" name="finishDate" value="${project.finishDate}"></div>
            </p>
            <p>
                <div style="margin-bottom: 5px">USER:</div>
                <div><input type="text" readonly="true" name="user" value="${project.user.login}"></div>
            </p>
        </td>

        <td valign="top">
            <a style="margin-bottom: 5px">TASKS OF PROJECT:</a>
            <table width="70%" border="1" cellpadding="5" style="border-collapse: collapse">
                <tr>
                    <td width="50">ID</td>
                    <td width="120">NAME</td>
                    <td width="200">DESCRIPTION</td>
                </tr>
                <c:forEach items="${tasks}" var="task">
                    <tr>
                        <td><c:out value="${task.id}"/></a></td>
                        <td><a href="/tasks/view/${task.id}"><c:out value="${task.name}"/></td>
                        <td><c:out value="${task.description}"/></td>
                    </tr>
                </c:forEach>
            </table>
        </td>
    </tr>
</table>

<jsp:include page="../include/_footer.jsp" />