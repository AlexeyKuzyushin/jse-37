<html>
    <head>
        <title>TASK MANAGER</title>
    </head>
    <style>
        h1 {
            font-size: 1.4em;
        }
    </style>

    <body>
        <table width="100%" height="100%" border="1" style="border-collapse: collapse">
            <tr>
                <td width="15%" height="35" nowrap="nowrap" align="center">
                    <a style="text-decoration: none;" href="/">TASK MANAGER</a>
                </td>
                <td width="90%" align="right">
                    <a style="text-decoration: none;" href="/projects">PROJECTS</a>
                    |
                    <a style="text-decoration: none;" href="/tasks">TASKS</a>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2" height="100%" valign="top" style="padding: 10px">